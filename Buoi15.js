var khuVucUuTien = function (khuVuc) {
  switch (khuVuc) {
    case "KV1": {
      return 2;
    }
    case "KV2": {
      return 1;
    }
    case "KV3": {
      return 0.5;
    }
    case "KVnone": {
      return 0;
    }
  }
};
var doiTuongUuTien = function (doiTuong) {
  switch (doiTuong) {
    case "DT1": {
      return 2.5;
    }
    case "DT2": {
      return 1.5;
    }
    case "DT3": {
      return 1;
    }
    case "DTnone": {
      return 0;
    }
  }
};
var ketQua = function (tongDiem, diemChuan) {
  if (tongDiem >= diemChuan) {
    KQ = 1;
  } else {
    KQ = 0;
  }
  return KQ;
};
document.getElementById("clickKQ").addEventListener("click", function () {
  var KVUuTien = document.querySelector(
    'input[name="khu_vuc_uu_tien"]:checked'
  ).value;
  console.log(KVUuTien);
  var DTUuTien = document.querySelector(
    'input[name="doi_tuong_uu_tien"]:checked'
  ).value;
  console.log(DTUuTien);
  var diemKVUuTien = khuVucUuTien(KVUuTien);
  console.log(diemKVUuTien);
  var diemDTUuTien = doiTuongUuTien(DTUuTien);
  console.log(diemDTUuTien);
  var diemChuan = document.getElementById("diem_chuan").value * 1;
  var diem1 = document.getElementById("diem_mon_1").value * 1;
  var diem2 = document.getElementById("diem_mon_2").value * 1;
  var diem3 = document.getElementById("diem_mon_3").value * 1;
  console.log(diem1, diem2, diem3);
  var tongDiem = diem1 + diem2 + diem3 + diemDTUuTien + diemKVUuTien;
  var ketQuaCuoiCung = ketQua(tongDiem, diemChuan);
  if (ketQuaCuoiCung === 0 || diem1 === 0 || diem2 === 0 || diem3 === 0) {
    document.getElementById(
      "KQ"
    ).innerHTML = `Điểm chuẩn là ${diemChuan} <br/> Tổng điểm của bạn là ${tongDiem} <br/>Bạn đã rớt rồi <br/> Cố gắng lên nha !!  `;
    document.getElementById("KQ").style.color = "blue";
  } else {
    document.getElementById(
      "KQ"
    ).innerHTML = `Điểm chuẩn là ${diemChuan} <br/> Tổng điểm của bạn là ${tongDiem} <br/>Xin chúc mừng <br/> Bạn đã đậu rồi `;
    document.getElementById("KQ").style.color = "green";
  }
});
//  Hết Bài 1
// Bài 2
var tinhTienTra = function (sokW) {
  soTienPhaiTra = null;
  if (sokW <= 50) {
    soTienPhaiTra = sokW * 500;
  } else if (sokW <= 100) {
    soTienPhaiTra = 50 * 500 + (sokW - 50) * 650;
  } else if (sokW <= 200) {
    soTienPhaiTra = 50 * 500 + 50 * 650 + (sokW - 100) * 850;
  } else if (sokW <= 350) {
    soTienPhaiTra = 50 * 500 + 50 * 650 + 100 * 850 + (sokW - 200) * 1100;
  } else {
    soTienPhaiTra =
      50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (sokW - 350) * 1300;
  }
  return soTienPhaiTra;
};
document.getElementById("tinh_KQ").addEventListener("click", function () {
  var soKw = document.getElementById("so_kW").value * 1;
  var soTienTra = tinhTienTra(soKw);
  var formatSoTien = Intl.NumberFormat("vi-VI", {
    style: "currency",
    currency: "VND",
  }).format(soTienTra);
  document.getElementById(
    "message"
  ).innerHTML = ` Tổng số tiền điện phải trả là ${formatSoTien}`;
  document.getElementById("message").style.color = "red";
});
